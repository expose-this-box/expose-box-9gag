const reactHMR = require('react-app-rewire-hot-loader');

module.exports = function overrides(config, env) {
	config = reactHMR(config, env);
	config.resolve.alias = {
		...config.resolve.alias,
		'react-dom': '@hot-loader/react-dom',
	};

	return config;
};
