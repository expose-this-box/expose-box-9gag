import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const columns = [
	{
		title: 'Type',
		dataIndex: 'searchType',
		key: 'searchType',
	},
	{
		title: 'Search Term',
		dataIndex: 'searchTerm',
		key: 'searchTerm',
	},
	{
		title: 'Counter',
		dataIndex: 'counter',
		key: 'counter',
	},
];


class Top10Table extends React.Component {

	render() {
		return (
			<div className="results-table">
				<Table
					loading={this.props.isLoading}
					columns={columns}
					dataSource={this.props.auth.top10Queries}
					rowKey={() => Math.random()}
				/>
			</div>
		);
	}
}

Top10Table.propTypes = {
	auth: PropTypes.object,
	isLoading: PropTypes.bool,
};


export default connect(
	(state) => ({
		...state,
	}),
	(dispatch) => ({

	}),
)(Top10Table);
