/* eslint-disable no-case-declarations */
/* eslint-disable react/no-multi-comp */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, Pagination } from 'antd';
import moment from 'moment';
import _ from 'lodash';

const columns = [
	{
		title: 'Title',
		dataIndex: 'title',
		key: 'title',
	},
	{
		title: 'Url',
		dataIndex: 'url',
		key: 'url',
		render: (text) => <a href={text} target="_blank" rel="noopener noreferrer">{text}</a>,
	},
	{
		title: 'NSFW',
		dataIndex: 'nsfw',
		key: 'nsfw',
		render: (nsfw) => (nsfw ? 'true' : 'false'),

	},
	{
		title: 'Created At',
		dataIndex: 'creationTs',
		key: 'creationTs',
		render: (creationTs) => <span>{moment(creationTs * 1000).format('YYYY-MM-DD HH-mm')}</span>,
	},
];

let currPage = 1;
class ResultsTable extends React.Component {

	onPaginate = (e) => {
		const isUp = e > currPage;
		currPage = e;
		this.props.paginate(isUp);
	}

	expandedRowRender = (dataObj) => {
		const columns = [
			{
				title: '',
				dataIndex: '1',
				key: 'one',
				render: (row, record) => {
					const title = _.first(_.keys(record)) || '';
					return _.capitalize(title);
				},
			},
			{
				title: '',
				dataIndex: '2',
				key: 'two',
				render: (row, record) => {
					const title = _.first(_.keys(record)) || '';
					switch (title) {
						case 'images':
							if (isPhoto) {
								const { url } = _.first(_.values(record.images)) || {};
								return (
									<img src={url} alt="" style={{ height: '200px' }} />
								);
							} else {
								const url = _.get(record, 'images.image460svwm.url', '');
								return (
									// eslint-disable-next-line jsx-a11y/media-has-caption
									<video src={url} controls style={{ height: '200px' }} />
								);
							}
						case 'upVoteCount':
						case 'downVoteCount':
						case 'commentsCount':
						case 'id':
							return _.first(_.values(record));
						default:
							break;
					}

				},
			},
		];

		const isPhoto = dataObj.type === 'Photo';

		const filteredData = _.pick(dataObj, ['images', 'id', 'upVoteCount', 'downVoteCount', 'commentsCount']);
		const data = _.map(filteredData, (value, field) => {
			return { [field]: value };
		});

		return <Table columns={columns} dataSource={data} rowKey={() => Math.random()} pagination={false} />;
	};

	render() {
		return (
			<div className="results-table">
				<Pagination onChange={this.onPaginate} total={Infinity} size="small" simple />

				<Table
					columns={columns} dataSource={this.props.gag.posts} rowKey={() => Math.random()} expandedRowRender={this.expandedRowRender}
				/>
			</div>
		);
	}
}

ResultsTable.propTypes = {
	gag: PropTypes.object,
	paginate: PropTypes.func.isRequired,
};


export default connect(
	(state) => ({
		...state,
	}),
	(dispatch) => ({

	}),
)(ResultsTable);
