import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
	Form, Icon, Input, Button, Select,
} from 'antd';

const { Option } = Select;

class Search9Gag extends React.Component {

	handleSubmit = (event) => {
		event.preventDefault();

		this.props.form.validateFields((err, formData) => {
			if (!err) {
				this.props.submitSearch(formData);
			}

		});
	}

	render() {
		const { form: { getFieldDecorator } } = this.props;

		return (
			<div
				className="Login" style={{
					display: 'flex', flexDirection: 'column', alignItems: 'center', marginBottom: '15px',
				}}
			>
				<Form layout="inline" onSubmit={this.handleSubmit} className="login-form">
					<Form.Item>
						{getFieldDecorator('searchTerm', {
							rules: [{ required: true, message: 'Please type a search term' }],
						})(
							<Input
								prefix={<Icon type="search" style={{ color: 'rgba(0,0,0,.25)' }} />}
								placeholder="Type your search here"
							/>,
						)}
					</Form.Item>
					<Form.Item>
						{getFieldDecorator('searchType', {
							initialValue: 'query',
							rules: [{ required: true, message: 'Please choose a type!' }],
						})(
							<Select style={{ width: 120 }}>
								<Option value="query">By Query</Option>
								<Option value="tag">By Tag</Option>
							</Select>,
						)}
					</Form.Item>
					<Form.Item>
						<Button type="primary" htmlType="submit" className="login-form-button">
							Search 9gag
						</Button>
					</Form.Item>
				</Form>
			</div>
		);
	}
}

Search9Gag.propTypes = {
	submitSearch: PropTypes.func.isRequired,
	form: PropTypes.object,
};

const WrappedSearch9Gag = Form.create({ name: 'search' })(Search9Gag);

export default connect()(WrappedSearch9Gag);
