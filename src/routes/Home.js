import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import _ from 'lodash';

import { search9Gag } from '../actions/gag';
import { updateTop10, getUpdatedTop10 } from '../actions/auth';

import Search9Gag from './components/search9Gag';
import ResultsTable from './components/resultsTable';
import Top10Table from './components/top10Table';
import './login.css';

class Home extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			currMode: 'search',
			searchType: '',
			searchTerm: '',
		};
	}

	componentDidMount() {
		this.props.getUpdatedTop10({ id: this.props.auth.id });
	}

	handleSubmitSearch = ({ searchType, searchTerm }) => {
		this.props.search9Gag({ searchType, searchTerm });
		this.props.updateTop10({ id: this.props.auth.id, searchType, searchTerm });
		this.setState((prevState) => ({
			...prevState,
			searchType,
			searchTerm,
		}));
	}

	handlePagination = (isUp) => {
		const { searchType, searchTerm } = this.state;
		let cursorToUse;
		if (isUp) {
			cursorToUse = this.props.gag.nextCursor;
		} else {
			if (this.props.gag.nextCursor <= 20) {
				cursorToUse = 0;
			}
			cursorToUse = this.props.gag.nextCursor - 20;
		}

		if (!searchTerm || !searchType) {
			return;
		}

		this.props.search9Gag({ searchType, searchTerm, cursorToUse });
	}

	handleToggleMode = () => {
		this.setState((prevState) => {
			const nextMode = prevState.currMode === 'search'
				? 'top10'
				: 'search';

			if (nextMode === 'top10') {
				this.props.getUpdatedTop10({ id: this.props.auth.id });
			}

			return { currMode: nextMode };
		});

	}

	render() {
		const isTop10 = this.state.currMode === 'top10';
		return (
			<div style={{ display: 'flex', flexDirection: 'column', marginBottom: '20px' }}>
				<Button onClick={this.handleToggleMode} size="large" style={{ marginBottom: '20px' }}>{_.capitalize(this.state.currMode)} mode active. Click to toggle</Button>
				{isTop10 && <Top10Table isLoading={this.props.auth.isLoadingTop10} />}
				{!isTop10 && <Search9Gag submitSearch={this.handleSubmitSearch} />}
				{!isTop10 && <ResultsTable paginate={this.handlePagination} />}
			</div>
		);
	}
}

Home.propTypes = {
	search9Gag: PropTypes.func.isRequired,
	updateTop10: PropTypes.func.isRequired,
	getUpdatedTop10: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	gag: PropTypes.object.isRequired,
};


export default connect(
	(state) => ({
		...state,
	}),
	(dispatch) => ({
		search9Gag: (data) => dispatch(search9Gag(data)),
		updateTop10: (data) => dispatch(updateTop10(data)),
		getUpdatedTop10: (data) => dispatch(getUpdatedTop10(data)),
	}),
)(Home);
