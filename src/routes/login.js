import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
	Redirect,
} from 'react-router-dom';
import {
	Form, Icon, Input, Button,
} from 'antd';

import { login } from '../actions/auth';
import './login.css';

class Login extends React.Component {

	handleSubmit = (event) => {
		event.preventDefault();

		this.props.form.validateFields((err, { username: name, password }) => {
			if (!err) {
				this.props.login({ name, password });
			}
		});
	}

	render() {
		const { form: { getFieldDecorator }, auth: { name } } = this.props;

		return (
			(name) ? (
				<Redirect
					to={{
						pathname: '/home',
					// state: { from: location }
					}}
				/>
			) :	(
				<div className="Login">
					<Form onSubmit={this.handleSubmit} className="login-form">
						<Form.Item>
							{getFieldDecorator('username', {
								rules: [{ required: true, message: 'Please input your username!' }],
							})(
								<Input
									prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
									placeholder="Username"
								/>,
							)}
						</Form.Item>
						<Form.Item>
							{getFieldDecorator('password', {
								rules: [{ required: true, message: 'Please input your Password!' }],
							})(
								<Input
									prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
									type="password"
									placeholder="Password"
								/>,
							)}
						</Form.Item>
						<Form.Item>
							<Button type="primary" htmlType="submit" className="login-form-button">
						Log in
							</Button>
						</Form.Item>
					</Form>
				</div>
			)
		);
	}
}

Login.propTypes = {
	login: PropTypes.func.isRequired,
	form: PropTypes.object,
	auth: PropTypes.object.isRequired,
};

const WrappedLogin = Form.create({ name: 'normal_login' })(Login);

export default connect(
	(state) => ({
		...state,
	}),
	(dispatch) => ({
		login: (data) => dispatch(login(data)),
	}),
)(WrappedLogin);
