import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const composeEnhanced = (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose);

export default function configureStore() {
	return createStore(
		rootReducer,
		composeEnhanced(applyMiddleware(thunk)),
	);
}
