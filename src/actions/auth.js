import _ from 'lodash';

export function login({ name, password }) {
	return async (dispatch) => {
		try {
			const result = await fetch('http://localhost:3001/auth/login', {
				method: 'POST',
				mode: 'cors',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({ name, password }),
			});

			const { message, name: me, id } = await result.json();

			if (message) {
				console.error('error: ', message);
				dispatch({
					type: 'LOGIN_FAIL',
				});
				return;
			}

			dispatch({
				type: 'LOGIN_SUCCESS',
				payload: { name: me, id },
			});
		} catch (e) {
			console.error('e is: ', e);
			//TODO: notifications
			// notification.error({
			// 	message: 'Error!',
			// 	description: e.message,
			// });
		}
	};
}

export function logout() {
	return (dispatch) => {
		dispatch({
			type: 'LOGOUT_SUCCESS',
			payload: {},
		});

	};
}

export function getUpdatedTop10({ id }) {
	return async (dispatch) => {
		try {

			dispatch({
				type: 'GET_QUERIES_START',
			});

			const result = await fetch('http://localhost:3001/users/get-top-queries', {
				method: 'POST',
				mode: 'cors',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({ id }),
			});

			const { message, results } = await result.json();

			if (message) {
				console.error('error: ', message);
				dispatch({
					type: 'GET_QUERIES_FAIL',
				});
				return;
			}

			const top10Queries = _.map(results.top10Queries, (query) => {
				const splitIndex = _.indexOf(query.k, '_');
				const searchType = query.k.slice(0, splitIndex);
				const searchTerm = query.k.slice(splitIndex + 1);

				return {
					counter: query.v,
					searchType,
					searchTerm,
				};
			});

			dispatch({
				type: 'GET_QUERIES_SUCCESS',
				payload: { top10Queries },
			});
		} catch (e) {
			console.error('e is: ', e);
			dispatch({
				type: 'GET_QUERIES_FAIL',
			});
			//TODO: notifications
			// notification.error({
			// 	message: 'Error!',
			// 	description: e.message,
			// });
		}
	};
}

export function updateTop10({ id, searchType, searchTerm }) {
	return async () => {
		try {
			const queryKey = `${searchType}_${searchTerm}`;

			await fetch('http://localhost:3001/users/update-top-queries', {
				method: 'POST',
				mode: 'cors',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({ id, queryKey }),
			});

		} catch (e) {
			console.error('e is: ', e);
			//TODO: notifications
			// notification.error({
			// 	message: 'Error!',
			// 	description: e.message,
			// });
		}
	};
}
