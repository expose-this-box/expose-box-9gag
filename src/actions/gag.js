export function search9Gag({ searchType, searchTerm, cursorToUse = 0 }) {
	return async (dispatch) => {
		try {
			const BASE_URL = 'https://9gag.com/v1';
			const isQuery = searchType === 'query';
			const urlPath = isQuery ? 'search-posts' : `tag-posts/tag/${searchTerm}`;
			const url = new URL(`${BASE_URL}/${urlPath}`);

			if (isQuery) {
				const params = { query: searchTerm, c: cursorToUse };
				url.search = new URLSearchParams(params).toString();
			}

			const result = await fetch(`https://cors-test-expose.herokuapp.com/${url}`, {
				method: 'GET',
				mode: 'cors',
				headers: {
					'Accept': 'application/json',
					'x-requested-with': 'https://9gag.com',
				},
			});

			const { meta: { status, errorCode }, data: { posts, nextCursor } } = await result.json();

			if (status === 'Success') {
				const { c } = parseQueryString(nextCursor);

				dispatch({
					type: 'SEARCH_9GAG_SUCCESS',
					payload: { posts, c },
				});
			} else {
				console.error('error: ', errorCode);
				dispatch({
					type: 'SEARCH_9GAG_FAIL',
				});
			}

		} catch (e) {
			console.error('e is: ', e);
			//TODO: notifications
			// notification.error({
			// 	message: 'Error!',
			// 	description: e.message,
			// });
		}
	};
}
export function searchByTag() {
	return (dispatch) => {
		dispatch({
			type: 'LOGOUT_SUCCESS',
			payload: {},
		});

	};
}

const parseQueryString = function(queryString = '') {
	const params = {}; let i; let l;

	// Split into key/value pairs
	const queries = queryString.split('&');

	// Convert the array of strings into an object
	for (i = 0, l = queries.length; i < l; i++) {
		const [key, val] = queries[i].split('=');
		params[key] = val;
	}

	return params;
};
