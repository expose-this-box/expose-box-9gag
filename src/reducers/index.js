import { combineReducers } from 'redux';
import auth from './auth';
import gag from './gag';

export default combineReducers({
	auth,
	gag,
});
