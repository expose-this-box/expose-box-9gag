const initial = {
	posts: [],
	nextCursor: 0,
};

export default (state = initial, action) => {
	switch (action.type) {
		case 'SEARCH_9GAG_SUCCESS':
			return {
				...state,
				posts: action.payload.posts,
				nextCursor: action.payload.c,
			};
		case 'SEARCH_9GAG_FAIL':
			return {
				...state,
			};
		default:
			return state;
	}
};
