const initial = {
	name: '',
	id: '',
	top10Queries: [],
};

export default (state = initial, action) => {
	switch (action.type) {
		case 'LOGIN_SUCCESS':
			return {
				...state,
				name: action.payload.name,
				id: action.payload.id,
			};
		case 'LOGIN_FAIL':
			return {
				...state,
				name: '',
			};
		case 'LOGOUT_SUCCESS':
			return {
				...state,
				name: '',
			};
		case 'GET_QUERIES_SUCCESS':
			return {
				...state,
				top10Queries: action.payload.top10Queries,
				isLoadingTop10: false,
			};
		case 'GET_QUERIES_START':
			return {
				...state,
				isLoadingTop10: true,
			};
		case 'GET_QUERIES_FAIL':
			return {
				...state,
				isLoadingTop10: false,
			};
		default:
			return state;
	}
};
