/* eslint-disable react/button-has-type */
/* eslint-disable react/no-multi-comp */
import React, { Component } from 'react';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
	Redirect,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { hot } from 'react-hot-loader';
import PropTypes from 'prop-types';
import { Menu } from 'antd';

import { logout } from './actions/auth';
import Login from './routes/login';
import Home from './routes/Home';
import './App.css';

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			current: 'login',
		};
	}

	handleClick = (e) => {
		if (e.key === 'logout') {
			this.props.logoutAction();
		}

		if (e.key !== 'name') {
			this.setState({
				current: e.key,
			});
		}
	};

	PrivateRoute = ({ children, ...rest }) => {
		return (
			<Route
				// eslint-disable-next-line react/jsx-props-no-spreading
				{...rest}
				render={({ location }) => (this.props.auth.name ? (
					children
				) : (
					<Redirect
						to={{
							pathname: '/login',
							state: { from: location },
						}}
					/>
				))}
			/>
		);
	}

	render() {
		const { name } = this.props.auth;

		return (
			<Router>
				<Menu onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal">
					{name && <Menu.Item key="name">Welcome, {name}!</Menu.Item>}
					{name ? (
						<Menu.Item key="logout">
							<Link to="/login">Logout</Link>
						</Menu.Item>
					) : (
						<Menu.Item key="login">
							<Link to="/login">Login</Link>
						</Menu.Item>
					)}
				</Menu>

				<div className="switch-wrapper">
					<Switch>
						<Route path="/login">
							<Login />
						</Route>
						<this.PrivateRoute path="/">
							<Home />
						</this.PrivateRoute>
					</Switch>
				</div>
			</Router>
		);
	}
}

App.propTypes = {
	auth: PropTypes.object.isRequired,
	logoutAction: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
	...state,
});

const mapDispatchToProps = (dispatch) => ({
	logoutAction: () => dispatch(logout()),
});

export default hot(module)(connect(mapStateToProps, mapDispatchToProps)(App));
